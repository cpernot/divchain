use rand::prelude::*;
use std::{
    sync::mpsc,
    time::{Duration, Instant},
};

const N: usize = 1000;

#[derive(Clone, Copy, Default)]
struct DejaVu {
    data: [usize; 16],
}
impl DejaVu {
    fn get(&self, i: usize) -> bool {
        let sz = usize::BITS as usize;
        self.data[i / sz] & 1 << (i % sz) != 0
    }
    fn set(&mut self, i: usize) {
        let sz = usize::BITS as usize;
        self.data[i / sz] |= 1 << (i % sz)
    }
    fn unset(&mut self, i: usize) {
        let sz = usize::BITS as usize;
        self.data[i / sz] &= !(1 << (i % sz))
    }
}

#[derive(Clone, Default)]
struct Chaine {
    dejavu: DejaVu,
    chaine: Vec<usize>,
}
impl std::ops::Deref for Chaine {
    type Target = Vec<usize>;

    fn deref(&self) -> &Self::Target {
        &self.chaine
    }
}
impl From<Vec<usize>> for Chaine {
    fn from(chaine: Vec<usize>) -> Self {
        let mut dejavu = DejaVu::default();
        for &x in &chaine {
            dejavu.set(x);
        }
        Self { dejavu, chaine }
    }
}
impl std::fmt::Display for Chaine {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let s = self
            .chaine
            .iter()
            .map(ToString::to_string)
            .collect::<Vec<_>>()
            .join("-");
        write!(f, "Longueur {}: {}", self.chaine.len(), s)
    }
}
impl Chaine {
    pub fn push(&mut self, x: usize) {
        self.chaine.push(x);
        self.dejavu.set(x);
    }
    pub fn pop(&mut self) {
        self.dejavu.unset(self.chaine.pop().unwrap());
    }
    pub fn has(&self, x: usize) -> bool {
        self.dejavu.get(x)
    }
}

struct State {
    div: Vec<Vec<usize>>,
    meilleur: Chaine,
    chaine: Chaine,
    recv: mpsc::Receiver<bool>,
    sender: mpsc::Sender<bool>,
}
impl State {
    pub fn init(seed: &[usize]) -> Self {
        let mut dejavu = DejaVu::default();
        for &x in seed {
            dejavu.set(x);
        }
        let mut div: Vec<Vec<usize>> = [Vec::new()].into();
        for a in 1..=N {
            div.push((1..=N).rev().filter(|x| a % x == 0 || x % a == 0).collect());
        }
        let (sender, recv) = mpsc::channel();
        Self {
            div,
            meilleur: Chaine::default(),
            chaine: seed.to_owned().into(),
            recv,
            sender,
        }
    }
    // Cherche un résultat le plus long possible.
    // La meilleure chaine est stockée dans self.max
    fn solve(&mut self, secs: u64) {
        let sender = self.sender.clone();
        // On créé un thread qui envoie dans le channel un false toutes les 500ms pendant `secs` secondes,
        // puis un signal true
        // false: remonte aléatoirement dans la chaine
        // true: termine la recherche
        let worker = std::thread::spawn(move || {
            let start = Instant::now();
            while start.elapsed().as_secs() < secs {
                std::thread::sleep(Duration::from_millis(500));
                sender.send(false).unwrap();
            }
            sender.send(true).unwrap();
        });
        self.resolv();
        worker.join().unwrap();
        // On vide tous les messages éventuellement contenus dans le channel
        self.recv.try_iter().fuse().for_each(drop);
    }
    // Réinitialise la chaine
    fn restart(&mut self, seed: &[usize]) {
        self.chaine = seed.to_owned().into();
    }
    fn resolv(&mut self) -> usize {
        let possib = self.avail();
        if possib.is_empty() {
            let v = self.enrichir();
            if v.len() > self.meilleur.len() {
                println!("{}\n", v);
                self.meilleur = v;
            }
        }
        if let Ok(b) = self.recv.try_recv() {
            if b {
                return self.chaine.len();
            }
            if let Some(n) = (1..self.chaine.len() - 1).choose(&mut thread_rng()) {
                return n - 1;
            }
        }
        for x in possib {
            self.chaine.push(x);
            let n = self.resolv();
            self.chaine.pop();
            if n > 0 {
                return n - 1;
            }
        }
        0
    }
    fn avail(&self) -> Vec<usize> {
        self.div[*self.chaine.last().unwrap()]
            .iter()
            .filter(|&&x| !self.chaine.has(x))
            .copied()
            .collect()
    }
    fn enrichir(&self) -> Chaine {
        let mut result = Chaine::default();
        let mut dejavu = self.chaine.dejavu;
        let first = *self.chaine.first().unwrap();
        let mut stack = self.chaine[1..].iter().copied().rev().collect::<Vec<_>>();
        result.push(first);
        while !stack.is_empty() {
            let last = *result.last().unwrap();
            if let Some(&m) = self.div[*stack.last().unwrap()]
                .iter()
                .find(|&&x| !dejavu.get(x) && (last % x == 0 || x % last == 0))
            {
                dejavu.set(m);
                stack.push(m);
            } else {
                result.push(stack.pop().unwrap());
            }
        }
        while let Some(&m) = self.div[*result.last().unwrap()]
            .iter()
            .find(|&&x| !dejavu.get(x))
        {
            result.push(m);
        }
        result
    }
}
fn main() {
    let start = std::env::args()
        .skip(1)
        .map(|s| s.parse())
        .collect::<Result<Vec<_>, _>>()
        .expect("Nombre attendu");
    if start.is_empty() {
        let mut s = State::init(&[1]);
        loop {
            for x in 10..=1000 {
                s.restart(&[x]);
                s.solve(20);
            }
        }
    } else {
        let mut s = State::init(&start);
        s.solve(20);
    }
}
