#!/usr/bin/env python3
T = list(map(int, input().split("-")))
def relate(a,b):
    return a % b == 0 or b % a == 0
def check(T: list[int]):
    for i in range(len(T)-1):
        if not relate(T[i], T[i+1]):
            print(T[i], T[i+1])
            return False
    return True
if check(T):
    print("OK")
else:
    print("Invalide")
